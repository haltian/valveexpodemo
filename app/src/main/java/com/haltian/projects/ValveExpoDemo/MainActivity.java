/*
 ***************************************************************************
 *
 Copyright (C) 2018 Haltian Oy. All rights reserved.
 *
 This source code can not be copied and/or distributed without the express
 permission of Haltian.
 *
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 *
 *
 ****************************************************************************/

package com.haltian.projects.ValveExpoDemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import static com.haltian.projects.ValveExpoDemo.CommonConstants.NOTIFICATION;
import static com.haltian.projects.ValveExpoDemo.CommonConstants.REQUEST_ENABLE_BT;
import static com.haltian.projects.ValveExpoDemo.CommonConstants.RAWDATA;

public class MainActivity extends AppCompatActivity implements ServiceConnection {

    private Context mContext;
    private BluetoothAdapter mBluetoothAdapter;
    private ScrollView mScrollView;

    private Handler mSplashHandler = new Handler();

    private static final int REQUEST_BT_PERMISSION = 2;
    private static String[] PERMISSIONS_BT = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private boolean mHasScanningRunning = false;

    private Handler restartBTLEHandler = new Handler();

    private int mValve2Angle = 180;
    private int mValve3Angle = 180;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.splash_screen).setVisibility(View.VISIBLE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mContext = this;

        mScrollView = findViewById(R.id.data_scroller);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not Supported", Toast.LENGTH_LONG).show();
            finish();
        }
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null)
            mBluetoothAdapter = bluetoothManager.getAdapter();

        mSplashHandler.postDelayed(hideSplashScreen, 3000);

        // Set fonts
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Black.otf");
        ((TextView) findViewById(R.id.valve2text)).setTypeface(type);
        ((TextView) findViewById(R.id.valve3text)).setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Regular.otf");
        ((TextView) findViewById(R.id.valvetext)).setTypeface(type);

        restartBTLEHandler.postDelayed(restartBTLERunnable, 10 * 60 * 1000); // every 10 minutes
    }

    private Runnable restartBTLERunnable = new Runnable() {
        @Override
        public void run() {
            stopScanning();
            scanForSensors();
            restartBTLEHandler.postDelayed(this, 10 * 60 * 1000); // every 10 minutes
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
    }

    @Override
    public void onDestroy() {
        stopScanning();
        super.onDestroy();
    }

    private Runnable hideSplashScreen = new Runnable() {

        @SuppressLint("InflateParams")
        public void run() {
            findViewById(R.id.splash_screen).setVisibility(View.GONE);
            if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS_BT, REQUEST_BT_PERMISSION);
                }
            }
            scanForSensors();
        }
    };

    @Override
    public void onBackPressed() {
        final Dialog exitQuery = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        exitQuery.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exitQuery.setCancelable(true);
        exitQuery.setContentView(R.layout.exit_app_query);
        ImageView cancel = exitQuery.findViewById(R.id.button_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                exitQuery.cancel();
            }
        });
        TextView logout = exitQuery.findViewById(R.id.button_exit);
        logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                exitQuery.cancel();
                finish();

            }
        });
        exitQuery.show();
        if (exitQuery.getWindow() != null) {
            exitQuery.getWindow().setDimAmount(0.5f);
            exitQuery.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
    }

    private void scanForSensors() {
        if (!mHasScanningRunning) {
            mHasScanningRunning = true;

            registerReceiver(receiver, new IntentFilter(NOTIFICATION));
            Intent intent = new Intent(this, ScanService.class);
            bindService(intent, this, Context.BIND_AUTO_CREATE);

            Intent service = new Intent(getApplicationContext(), ScanService.class);
            getApplicationContext().startService(service);
        }
    }

    private void stopScanning() {
        if (mHasScanningRunning) {
            mHasScanningRunning = false;

            unbindService(this);
            unregisterReceiver(receiver);
            Intent service = new Intent(getApplicationContext(), ScanService.class);
            getApplicationContext().stopService(service);
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String rawData = bundle.getString(RAWDATA);
                if (rawData != null && rawData.contains("RESULT")) {
                    Log.d("Raw", rawData);
                    String[] items = rawData.split(" ");
                    if (items.length == 3) {
                        int value = Integer.parseInt(items[2]) / 10;
                        //if (value > 180)
                        //value -= 360;

                        TextView valueView = null;
                        ImageView valveView = null;
                        int valueColor = ContextCompat.getColor(context, R.color.white);

                        switch (items[1]) {
                            case "200088":
                                valueView = findViewById(R.id.valve2text);
                                valveView = findViewById(R.id.valve2);
                                value = -(value - 184); // 184-0 - 97-90
                                break;
                            case "200095":
                                valueView = findViewById(R.id.valve3text);
                                valveView = findViewById(R.id.valve3);
                                value = -(value - 179); // 179-0 - 91-90
                                break;
                        }

                        String valueString = getString(R.string.valve_value_text, value);
                        if (value < 10) {
                            value = 0;
                            valueColor = ContextCompat.getColor(context, R.color.green);
                            valueString = getString(R.string.open_text);
                        }
                        if (value > 80) {
                            value = 90;
                            valueColor = ContextCompat.getColor(context, R.color.red);
                            valueString = getString(R.string.closed_text);
                        }
                        int valveAngle = value + 90;

                        if (valueView != null) {
                            valueView.setTextColor(valueColor);
                            valueView.setText(valueString);
                        }

                        if (valveView != null) {
                            int rotate = 0;
                            if (valveView == findViewById(R.id.valve2))
                                rotate = valveAngle - mValve2Angle;
                            if (valveView == findViewById(R.id.valve3))
                                rotate = valveAngle - mValve3Angle;

                            valveView.animate()
                                    .rotationBy(rotate)
                                    .setDuration(500)
                                    .start();

                            if (valveView == findViewById(R.id.valve2))
                                mValve2Angle += rotate;
                            if (valveView == findViewById(R.id.valve3))
                                mValve3Angle += rotate;
                        }
                    }
                } else {
                    TextView data = findViewById(R.id.data);
                    data.append(rawData + "\n");
                    mScrollView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //replace this line to scroll up or down
                            mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    }, 100L);
                }
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {

            case REQUEST_BT_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED))
                    finish();
                // permission was granted, yay!
                stopScanning();
                scanForSensors();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
            } else {
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS_BT, REQUEST_BT_PERMISSION);
                }
            }
        }
    }
}
