package com.haltian.projects.ValveExpoDemo;

public class CommonConstants {

    public static final String LOG_TAG = "WP BLE";
    public static final boolean DISPLAY_TRACES = true;

    static final int REQUEST_ENABLE_BT = 1;

    //Intent extras
    static final String RAWDATA = "com.haltian.projects.ValveExpoDemo.ble.scan.result.rawdata";
    static final String NOTIFICATION = "com.haltian.projects.ValveExpoDemo.ble.service.receiver";
}
