package com.haltian.projects.ValveExpoDemo;

import java.util.HashMap;

public class Message {
    int mMaxIndexes = -1;
    int mIndexCount = 0;

    int mSeqId = -1;
    int mPayloadLength = 0;

    HashMap<Integer, byte[]> mData = new HashMap<>();
}
