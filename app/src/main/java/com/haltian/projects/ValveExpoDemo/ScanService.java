package com.haltian.projects.ValveExpoDemo;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.SparseArray;

import org.json.JSONArray;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.engines.ChaCha7539Engine;
import org.spongycastle.crypto.Mac;
import org.spongycastle.crypto.macs.Poly1305;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.crypto.InvalidCipherTextException;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.util.Arrays;
import org.spongycastle.util.Pack;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.io.InputStream;
import java.io.ByteArrayInputStream;

import com.upokecenter.cbor.CBORObject;

import static com.haltian.projects.ValveExpoDemo.CommonConstants.NOTIFICATION;
import static com.haltian.projects.ValveExpoDemo.CommonConstants.RAWDATA;

public class ScanService extends Service {

    private final IBinder mBinder = new ScanServiceBinder();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mLEScanner;

    private byte[] mPrevEncryptedPayload = null;

    private SparseArray<Message> mMessages = new SparseArray<>();

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    int mPreviousSequence = 0;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null)
            mBluetoothAdapter = bluetoothManager.getAdapter();

        mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();
        List<ScanFilter> filters = new ArrayList<>();
        mLEScanner.startScan(filters, settings, mScanCallback);
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    private class ScanServiceBinder extends Binder {
        @SuppressWarnings("unused")
        ScanService getService() {
            return ScanService.this;
        }
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            int nodeid = 246828;
            int wp_network = 11917080;
            ByteBuffer macbuf = ByteBuffer.allocate(3 + 3);
            macbuf.put((byte) ((nodeid) & 0xff));
            macbuf.put((byte) ((nodeid >>> 8) & 0xff));
            macbuf.put((byte) ((nodeid >>> 16) & 0xff));
            macbuf.put((byte) ((wp_network) & 0xff));
            macbuf.put((byte) ((wp_network >>> 8) & 0xff));
            macbuf.put((byte) ((wp_network >>> 16) & 0xff));
            String macString = macStringFromMacBuffer(macbuf.array()); //TODO: Precompute
            if (result.getScanRecord() != null && result.getDevice() != null && result.getDevice().getAddress() != null) {
                if (result.getDevice().getAddress().equalsIgnoreCase(macString)) {
                    //Trace.trace("Address: " + result.getDevice().getAddress());
                    String addressString = result.getDevice().getAddress().replace(":", "");
                    String net = String.valueOf(Integer.parseInt(addressString.substring(0, 6), 16));
                    String node = String.valueOf(Integer.parseInt(addressString.substring(6, 12), 16));

                    //Trace.trace("Node id: " + node);
                    //Trace.trace("WP network: " + net);

                    byte[] rawData = result.getScanRecord().getBytes();
                    //Trace.trace("Raw data: " + toHexadecimal(rawData));
                    int length = rawData[0] - 3;
                    //int dataType = rawData[1];
                    int maxIndex = rawData[2] >> 4;
                    int currentIndex = rawData[2] & 0x0F;
                    int dataSeqNum = rawData[3] & 0xFF;

                    //Trace.trace("Raw data length: " + length);
                    //Trace.trace("Raw data dataType: " + dataType);
                    //Trace.trace("currentIndex: " + currentIndex);
                    //Trace.trace("dataSeqNum: " + dataSeqNum);
                    //Trace.trace("mMaxIndex: " + mMaxIndex);

                    if (dataSeqNum == mPreviousSequence)
                        Trace.trace("dataSeqNum: " + dataSeqNum);
                    else
                        Trace.trace("\nNEW dataSeqNum: " + dataSeqNum);
                    mPreviousSequence = dataSeqNum;

                    Trace.trace("Node id: " + node + " WP network: " + net);

                    Trace.trace("Index: " + currentIndex + " / " + maxIndex);

                    byte[] dataArray = new byte[length];
                    System.arraycopy(rawData, 4, dataArray, 0, dataArray.length);
                    //Trace.trace("dataArray: " + toHexadecimal(dataArray));

                    Message message = mMessages.get(dataSeqNum);
                    if (message == null) {
                        message = new Message();
                        message.mIndexCount = 1;
                        message.mMaxIndexes = maxIndex;
                        message.mSeqId = dataSeqNum;
                        message.mData.put(currentIndex, dataArray);
                        message.mPayloadLength = dataArray.length;
                        mMessages.put(dataSeqNum, message);
                    } else {
                        byte[] data = message.mData.get(currentIndex);
                        if (data == null) {
                            message.mData.put(currentIndex, dataArray);
                            message.mIndexCount++;
                            message.mPayloadLength += dataArray.length;
                        }
                    }

                    if (message.mIndexCount > message.mMaxIndexes) {
                        ByteBuffer buffer = ByteBuffer.allocate(message.mPayloadLength);

                        for (int i = 0; i < message.mData.size(); i++) {
                            buffer.put(message.mData.get(i));
                        }

                        byte[] encryptedPayload = buffer.array();

                        if (mPrevEncryptedPayload == null ||
                                !Arrays.constantTimeAreEqual(mPrevEncryptedPayload, encryptedPayload)) {
                            //Send payload to the UI to display
                            //publishResults(" ");
                            publishResults("\nNetwork:" + net + " GW Node:" + node + " SeqId:" + message.mSeqId);
                            //publishResults("Encrypted Payload: " + toHexadecimal(encryptedPayload));
                            mPrevEncryptedPayload = encryptedPayload;
                            mMessages.remove(message.mSeqId);

                            //Try to decrypt the payload
                            try {
                                byte[] wp_cipher_key = hexToByteArray("4307e2d2eecfae7f215b3247e766744e");
                                byte[] wp_auth_key = hexToByteArray("c47f062544b122d4ae53a476f374143f");

                                ByteBuffer keybuf = ByteBuffer.allocate(16 + 16 + 3 + 3);
                                keybuf.put(wp_cipher_key);
                                keybuf.put(wp_auth_key);
                                keybuf.put(macbuf.array());

                                String key = computeHash(keybuf.array()); //TODO: Precompute (this must be precomputed in cloud, application should not get wp_cipher_key/wp_auth_key directly)
                                //Trace.trace("enckey:" + key);
                                //<wp_cipher_key>:<wp_auth_key>:<sink_node_id>:<wp_network_id>)

                                byte[] plaintext = decryptChacha20Poly1305(hexToByteArray(key), macbuf.array(), buffer.array());

                                //Trace.trace("decrypted:" + toHexadecimal(plaintext));

                                //publishResults("Decrypted Payload: " + toHexadecimal(plaintext));

                                int srcNodeId;
                                int messageAge;

                                srcNodeId = ((int) plaintext[0] & 0xff);
                                srcNodeId += ((int) plaintext[1] & 0xff) << 8;
                                srcNodeId += ((int) plaintext[2] & 0xff) << 16;
                                messageAge = (int) plaintext[3];
                                messageAge += (int) plaintext[4] << 8;

                                //publishResults("Source Node:" + srcNodeId + " Message Age: " + messageAge);

                                try (InputStream stream = new ByteArrayInputStream(plaintext, 5, plaintext.length - 5)) {
                                    // Read the CBOR object from the stream
                                    CBORObject cbor = CBORObject.Read(stream);
                                    // If the end of the stream was reached.
                                    if (stream.available() == 0) {
                                        publishResults("Source Node:" + srcNodeId + " Message Age: " + messageAge + "\nMessage: " + cbor.toString());
                                        //Trace.trace("Message: " + cbor.toString());
                                        try {
                                            JSONArray jsonMessage = new JSONArray(cbor.ToJSONString());
                                            //Trace.trace("JSON: " + jsonMessage);
                                            for (int i = 0; i < jsonMessage.length(); i++) {
                                                try {
                                                    int value = jsonMessage.getJSONObject(i).getInt("190"); // Get angle
                                                    publishResults("RESULT " + srcNodeId + " " + value);
                                                    //Trace.trace("RESULT " + srcNodeId + " " + value);
                                                    break;
                                                } catch (Exception e) {
                                                    // No output in case of missing angle value
                                                }
                                            }
                                        } catch (Exception e) {
                                            Trace.trace(e.getMessage());
                                        }
                                    }
                                    // Else: The end of the stream wasn't reached yet.
                                }

                            } catch (Exception e) {
                                Trace.trace(e.getMessage());
                                publishResults("Failed to decrypt!");
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
        }

        @Override
        public void onScanFailed(int errorCode) {
            Trace.trace("Scan Failed - Error Code: " + errorCode);
        }
    };

    private static String macStringFromMacBuffer(byte[] mac) {
        StringBuilder sb = new StringBuilder();
        for (int i = mac.length - 1; i >= 0; i--) {
            int b = mac[i] & 0xff;
            if (Integer.toHexString(b).length() == 1)
                sb.append("0");
            sb.append(Integer.toHexString(b));
            if (i > 0)
                sb.append(":");
        }
        return sb.toString().toUpperCase();
    }

    private void publishResults(String info) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RAWDATA, info);
        sendBroadcast(intent);
    }

    private static byte[] decryptChacha20Poly1305(byte[] key, byte[] aad, byte[] iv_data_tag)
            throws InvalidCipherTextException {
        ChaCha7539Engine engine = new ChaCha7539Engine();
        CipherParameters chacha_cp = new KeyParameter(key);
        Mac mac = new Poly1305();
        byte[] iv = new byte[12];
        byte[] data = new byte[iv_data_tag.length - 12 - 16];
        byte[] out = new byte[data.length];
        byte[] tag = new byte[16];
        byte[] poly1305_key = new byte[64];
        byte[] zero_bytes = new byte[64];
        byte[] aad_len_buf = Pack.longToLittleEndian(aad.length & 0xFFFFFFFFL);
        byte[] data_len_buf = Pack.longToLittleEndian(data.length & 0xFFFFFFFFL);

        System.arraycopy(iv_data_tag, 0, iv, 0, 12);
        System.arraycopy(iv_data_tag, 12, data, 0, data.length);
        System.arraycopy(iv_data_tag, 12 + data.length, tag, 0, 16);
        //Trace.trace("iv:" + toHexadecimal(iv));
        //Trace.trace("data:" + toHexadecimal(data));
        //Trace.trace("tag:" + toHexadecimal(tag));

        engine.init(false, new ParametersWithIV(chacha_cp, iv));

        // Generate Poly1305 key
        engine.processBytes(zero_bytes, 0, 64, poly1305_key, 0); // first Chacha20 block used as poly1305 key
        mac.init(new KeyParameter(poly1305_key, 0, 32)); // first 32 bytes used, last 32 bytes discarded

        // digest additional authenticated data
        mac.update(aad, 0, aad.length);

        // pad AAD to 16 bytes with zero input
        if ((aad.length & 15) > 0) {
            mac.update(zero_bytes, 0, 16 - (aad.length & 15));
        }

        // digest ciphertext
        mac.update(data, 0, data.length);

        // pad ciphertext to 16 bytes with zero input
        if ((data.length & 15) > 0) {
            mac.update(zero_bytes, 0, 16 - (data.length & 15));
        }

        // digest aad and data lengths
        mac.update(aad_len_buf, 0, aad_len_buf.length);
        mac.update(data_len_buf, 0, data_len_buf.length);

        // calculate tag
        byte[] calc_tag = new byte[mac.getMacSize()];
        mac.doFinal(calc_tag, 0);
        //Trace.trace("calc_tag:" + toHexadecimal(calc_tag));

        // compare tags
        if (!Arrays.constantTimeAreEqual(calc_tag, tag)) {
            throw new InvalidCipherTextException("mac check in chacha20poly1305 failed");
        }

        // decrypt
        engine.processBytes(data, 0, data.length, out, 0);

        return out;
    }

    @SuppressWarnings("unused")
    private static String toHexadecimal(byte[] digest) {
        StringBuilder sb = new StringBuilder();

        for (byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) sb.append("0");
            sb.append(Integer.toHexString(b));
        }
        return sb.toString();
    }

    public String computeHash(byte[] inputBytes) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.reset();

        byte[] byteData = digest.digest(inputBytes);
        StringBuilder sb = new StringBuilder();

        for (byte dataByte : byteData) {
            sb.append(Integer.toString((dataByte & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static byte[] hexToByteArray(String hex) {
        hex = hex.length() % 2 != 0 ? "0" + hex : hex;

        byte[] b = new byte[hex.length() / 2];

        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(hex.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    @Override
    public void onDestroy() {
        mLEScanner.stopScan(mScanCallback);
    }
}
